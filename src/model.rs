use std::path::Path;

use cgmath::{InnerSpace, SquareMatrix};
use itertools::izip;

use crate::math::types::*;
use crate::texture::Texture;

pub struct Model {
    mesh: tobj::Mesh,
    /// Provides per-fragment color values.
    diffuse_texture: Texture,
    /// Provides per-fragment normal vectors.
    normal_map: Texture,
    /// Provides per-pixel specular exponents (i.e., shininess).
    specular_map: Texture,
    transform: Mat4f,
}

pub struct Face {
    pub vertices: [Vertex; 3],
}

pub struct Vertex {
    pub position: Vec3f,
    pub normal: Vec3f,
    pub tex_coord: Vec2f,
}

impl Model {
    /// When `force_gen_normals` is set, even if normals are supplied by the model, we generate them
    /// from the model's positions.
    pub fn new(
        model_filename: &str,
        diffuse_tex_filename: &str,
        normal_map_filename: &str,
        specular_map_filename: &str,
        force_gen_normals: bool,
    ) -> Self {
        let mesh = {
            let (models, _) = tobj::load_obj(Path::new(model_filename)).unwrap();
            assert_eq!(models.len(), 1);
            models.into_iter().next().unwrap().mesh
        };
        let diffuse_texture = Texture::load(diffuse_tex_filename);
        let normal_map = Texture::load(normal_map_filename);
        let specular_map = Texture::load(specular_map_filename);

        let mut result = Self {
            mesh,
            diffuse_texture,
            normal_map,
            specular_map,
            transform: Mat4f::identity(),
        };
        if result.mesh.normals.is_empty() || force_gen_normals {
            result.mesh.normals = result.gen_normals();
        }
        result
    }

    fn gen_normals(&self) -> Vec<f32> {
        // For each vertex, we interpolate over adjacent faces' normals to get per-vertex
        // normals (where we weight the interpolation by the size of the face).

        // Accumulate cross products into each result normal.
        let mut result = vec![
            Vec3f {
                x: 0.0,
                y: 0.0,
                z: 0.0
            };
            self.mesh.positions.len()
        ];
        for pos_vert_idx in self.mesh.indices.iter() {
            for face_vert_indices in self.mesh.indices.chunks(3) {
                if face_vert_indices.iter().any(|idx| idx == pos_vert_idx) {
                    // This face shares an index with `pos_vert_idx` (i.e., it's adjacent).
                    let face_positions: Vec<Vec3f> = face_vert_indices
                        .iter()
                        .map(|idx| Vec3f {
                            x: self.mesh.positions[(idx * 3) as usize],
                            y: self.mesh.positions[(idx * 3 + 1) as usize],
                            z: self.mesh.positions[(idx * 3 + 2) as usize],
                        })
                        .collect();
                    // Use the cross product to get this face's weighted contribution to the final
                    // normal.
                    let cross_prod_result = (face_positions[1] - face_positions[0])
                        .cross(face_positions[2] - face_positions[0]);
                    result[*pos_vert_idx as usize] += cross_prod_result;
                }
            }
        }

        // Normalize the accumulated vectors and flatten from vectors to floats.
        result
            .into_iter()
            .flat_map(|v| {
                let norm = v.normalize();
                vec![norm.x, norm.y, norm.z]
            })
            .collect()
    }

    pub fn faces<'a>(&'a self) -> impl Iterator<Item = Face> + 'a {
        izip!(self.positions(), self.normals(), self.tex_coords()).map(|(p, n, t)|
            Face {
                vertices: array_init::array_init(|i| {
                    Vertex {
                        position: p[i],
                        normal: n[i],
                        tex_coord: t[i],
                    }
                }),
            }
        )
    }

    pub fn positions<'a>(&'a self) -> impl Iterator<Item = [Vec3f; 3]> + 'a {
        self.mesh.indices.chunks(3).map(move |idxs| {
            array_init::from_iter(idxs.iter().map(|idx| Vec3f {
                x: self.mesh.positions[(idx * 3) as usize],
                y: self.mesh.positions[(idx * 3 + 1) as usize],
                z: self.mesh.positions[(idx * 3 + 2) as usize],
            }))
            .unwrap()
        })
    }

    pub fn normals<'a>(&'a self) -> impl Iterator<Item = [Vec3f; 3]> + 'a {
        self.mesh.indices.chunks(3).map(move |idxs| {
            array_init::from_iter(idxs.iter().map(|idx| Vec3f {
                x: self.mesh.normals[(idx * 3) as usize],
                y: self.mesh.normals[(idx * 3 + 1) as usize],
                z: self.mesh.normals[(idx * 3 + 2) as usize],
            }))
            .unwrap()
        })
    }

    pub fn tex_coords<'a>(&'a self) -> impl Iterator<Item = [Vec2f; 3]> + 'a {
        self.mesh.indices.chunks(3).map(move |idxs| {
            array_init::from_iter(idxs.iter().map(|idx| Vec2f {
                x: self.mesh.texcoords[(idx * 2) as usize],
                y: self.mesh.texcoords[(idx * 2 + 1) as usize],
            }))
            .unwrap()
        })
    }

    pub fn diffuse_texture(&self) -> &Texture {
        &self.diffuse_texture
    }

    pub fn normal_map(&self) -> &Texture {
        &self.normal_map
    }

    pub fn specular_map(&self) -> &Texture {
        &self.specular_map
    }

    pub fn transform(&self) -> &Mat4f {
        &self.transform
    }
}
