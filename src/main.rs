extern crate array_init;
extern crate cgmath;
extern crate image;
extern crate itertools;
extern crate tobj;

pub mod camera;
pub mod color;
pub mod math;
pub mod model;
pub mod render;
pub mod texture;

use std::env;

use crate::model::Model;
use crate::render::Renderer;

const DEFAULT_OUT_FILENAME: &'static str = "output.png";

const WIDTH: u32 = 800;
const HEIGHT: u32 = 800;

fn main() {
    // Use the first argument to the program as the output's file name.  If none is given, use
    // `DEFAULT_OUT_FILENAME`.
    let out_filename = {
        let args: Vec<String> = env::args().collect();
        if args.len() == 2 {
            args[1].clone()
        } else {
            String::from(DEFAULT_OUT_FILENAME)
        }
    };

    let mut renderer = Renderer::new(WIDTH, HEIGHT);
    let model = Model::new(
        "res/african_head/model.obj",
        "res/african_head/diffuse.tga",
        "res/african_head/norm_map_tangent.tga",
        "res/african_head/spec.tga",
        false,
    );

    renderer.render(&model);

    renderer.save(&out_filename);
}
