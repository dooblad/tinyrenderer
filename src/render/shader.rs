use cgmath::{InnerSpace, Matrix, SquareMatrix};

use crate::camera::Camera;
use crate::math::matrix::*;
use crate::math::types::*;
use crate::math::vector;
use crate::model::{Model, Vertex};
use crate::texture::Texture;

pub trait Shader {
    fn set_uniforms(&mut self, light_dir: &Vec3f, proj_mat: &Mat4f, camera: &Camera, model: &Model);
    fn vertex(&mut self, vert_num: usize, vertex: &Vertex) -> Vec4f;
    fn fragment(&self, bary_coords: &Vec3f, args: &FragShaderArgs) -> Option<Vec3f>;
}

pub struct FragShaderArgs<'a> {
    pub diffuse_tex: &'a Texture,
    pub normal_map: &'a Texture,
    pub specular_map: &'a Texture,
}

pub struct DepthShader {
    /// Projection * View * Model
    pub uniform_mvp: Mat4f,
    /// Positions in Normalized Device Coordinates
    pub varying_ndc_pos: [Vec3f; 3],
}

impl DepthShader {
    pub fn new() -> Self {
        Self {
            uniform_mvp: Mat4f::identity(),
            varying_ndc_pos: [Vec3f::new(0.0, 0.0, 0.0); 3],
        }
    }
}

impl Shader for DepthShader {
    fn set_uniforms(&mut self, _: &Vec3f, proj_mat: &Mat4f, camera: &Camera, model: &Model) {
        self.uniform_mvp = proj_mat * camera.view_mat() * model.transform();
    }

    fn vertex(&mut self, vert_num: usize, vertex: &Vertex) -> Vec4f {
        let clip_pos = self.uniform_mvp * vertex.position.extend(1.0);
        self.varying_ndc_pos[vert_num] = clip_pos.xyz() / clip_pos.w;
        clip_pos
    }

    fn fragment(&self, bary_coords: &Vec3f, args: &FragShaderArgs) -> Option<Vec3f> {
        let ndc_pos =
            self.varying_ndc_pos[0] * bary_coords[0]
                + self.varying_ndc_pos[1] * bary_coords[1]
                + self.varying_ndc_pos[2] * bary_coords[2];
        Some(Vec3f::new(ndc_pos.z / 2.0, ndc_pos.z / 2.0, ndc_pos.z / 2.0))
    }
}

pub struct MainShader {
    /// Projection * View * Model
    pub uniform_mvp: Mat4f,
    /// Inverse Transpose of MVP
    pub uniform_mvp_it: Mat4f,
    pub uniform_light_dir: Vec3f,
    pub varying_uv: [Vec2f; 3],
    pub varying_norm: [Vec3f; 3],
    /// Positions in Normalized Device Coordinates
    pub varying_ndc_pos: [Vec3f; 3],
}

impl MainShader {
    pub fn new() -> Self {
        Self {
            uniform_mvp: Mat4f::identity(),
            uniform_mvp_it: Mat4f::identity(),
            uniform_light_dir: Vec3f::new(0.0, 0.0, 0.0),
            varying_uv: [Vec2f::new(0.0, 0.0); 3],
            varying_ndc_pos: [Vec3f::new(0.0, 0.0, 0.0); 3],
            varying_norm: [Vec3f::new(0.0, 0.0, 0.0); 3],
        }
    }
}

impl Shader for MainShader {
    fn set_uniforms(&mut self, light_dir: &Vec3f, proj_mat: &Mat4f, camera: &Camera, model: &Model) {
        // Set uniforms.
        self.uniform_mvp = proj_mat * camera.view_mat() * model.transform();
        // `transpose` is an in-place op, so we need to clone.
        self.uniform_mvp_it = self
            .uniform_mvp
            .clone()
            .transpose()
            .invert()
            .unwrap();
        self.uniform_light_dir = light_dir.clone();
    }

    fn vertex(
        &mut self,
        vert_num: usize,
        vertex: &Vertex,
    ) -> Vec4f {
        let clip_pos = self.uniform_mvp * vertex.position.extend(1.0);
        self.varying_uv[vert_num] = vertex.tex_coord;
        self.varying_norm[vert_num] = vertex.normal;
        self.varying_ndc_pos[vert_num] = clip_pos.xyz() / clip_pos.w;
        clip_pos
    }

    fn fragment(
        &self,
        bary_coords: &Vec3f,
        args: &FragShaderArgs,
    ) -> Option<Vec3f> {
        const AMBIENT_COL: Vec3f = Vec3f {
            x: 0.1,
            y: 0.1,
            z: 0.1,
        };
        const DIFFUSE_COEFF: f32 = 1.0;
        const SPECULAR_COEFF: f32 = 0.6;

        let (diffuse_col, normal, specular_exp) = self.sample_textures(bary_coords, args);

        let screen_norm = (self.uniform_mvp_it * normal.extend(0.0)).xyz().normalize();
        let screen_light_dir = (self.uniform_mvp * self.uniform_light_dir.extend(0.0))
            .xyz()
            .normalize();

        let diffuse_comp = Self::diffuse_component(&screen_norm, &screen_light_dir, &diffuse_col);
        let specular_comp =
            Self::specular_component(&screen_norm, &screen_light_dir, &diffuse_col, specular_exp);

        let frag_col = AMBIENT_COL + DIFFUSE_COEFF * diffuse_comp + SPECULAR_COEFF * specular_comp;
        Some(frag_col)
    }
}

impl MainShader {
    fn sample_textures(
        &self,
        bary_coords: &Vec3f,
        args: &FragShaderArgs,
    ) -> (Vec3f, Vec3f, f32) {
        let FragShaderArgs { diffuse_tex, normal_map, specular_map } = args;
        let tex_coord = bary_coords[0] * self.varying_uv[0]
            + bary_coords[1] * self.varying_uv[1]
            + bary_coords[2] * self.varying_uv[2];
        let diffuse_col = diffuse_tex.sample(tex_coord).xyz();
        let normal = {
            let base_normal = bary_coords[0] * self.varying_norm[0]
                + bary_coords[1] * self.varying_norm[1]
                + bary_coords[2] * self.varying_norm[2];
            let bn = base_normal;
            let darboux = {
                let a = self.varying_ndc_pos[1] - self.varying_ndc_pos[0];
                let b = self.varying_ndc_pos[2] - self.varying_ndc_pos[0];
                Mat3f::row_maj_new(a.x, a.y, a.z, b.x, b.y, b.z, bn.x, bn.y, bn.z)
            };
            let darboux_inv = darboux.invert().unwrap();
            let i = (darboux_inv
                * Vec3f::new(
                    self.varying_uv[1][0] - self.varying_uv[0][0],
                    self.varying_uv[2][0] - self.varying_uv[0][0],
                    0.0,
                ))
            .normalize();
            let j = (darboux_inv
                * Vec3f::new(
                    self.varying_uv[1][1] - self.varying_uv[0][1],
                    self.varying_uv[2][1] - self.varying_uv[0][1],
                    0.0,
                ))
            .normalize();

            #[rustfmt::skip]
            let change_of_basis =
                Mat3f::row_maj_new(
                    i.x, j.x, bn.x,
                    i.y, j.y, bn.y,
                    i.z, j.z, bn.z);
            //// Map from [0, 1] to [-1, 1].
            let darboux_norm = (2.0 * normal_map.sample(tex_coord).xyz())
                .map(|v| v - 1.0)
                .normalize();
            change_of_basis * darboux_norm
        };
        let specular_exp = specular_map.sample(tex_coord)[0] * 255.0;
        (diffuse_col, normal, specular_exp)
    }

    fn diffuse_component(
        screen_norm: &Vec3f,
        screen_light_dir: &Vec3f,
        diffuse_col: &Vec3f,
    ) -> Vec3f {
        screen_norm.dot(screen_light_dir.clone()) * diffuse_col
    }

    fn specular_component(
        screen_norm: &Vec3f,
        screen_light_dir: &Vec3f,
        diffuse_col: &Vec3f,
        specular_exp: f32,
    ) -> Vec3f {
        // Reflect about `screen_norm`.
        let screen_reflection = vector::reflect(&screen_light_dir, &screen_norm);
        // TODO: We shouldn't just be taking the `z` component.  We should be passing the eye
        // position and the varying position.  But we're also working in screen space here, so idk.
        screen_reflection.z.max(0.0).powf(specular_exp) * diffuse_col
    }
}
