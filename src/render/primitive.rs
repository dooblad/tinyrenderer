use cgmath::InnerSpace;

use crate::color::*;
use crate::math::Clamp;
use crate::math::types::*;
use crate::math::vector::*;
use super::RenderContext;
use super::shader::FragShaderArgs;

/// Receives `verts` in clip coordinates.
pub fn triangle(
    clip_verts: &[Vec4f; 3],
    args: FragShaderArgs,
    render_ctx: &mut RenderContext,
) {
    // First convert to normalized device coordinates.
    let screen_verts: [Vec2f; 3] = array_init::array_init(|i| {
        // Do perspective division.
        let ndc_vert = clip_verts[i] / clip_verts[i].w;
        // Map to screen coordinates.
        (render_ctx.viewport_mat * ndc_vert).xy()
    });
    let bb = bounding_box(&screen_verts, Some(render_ctx.target_bounds()));
    for y in bb[0].y..=bb[1].y {
        for x in bb[0].x..=bb[1].x {
            point_in_triangle(
                x,
                y,
                clip_verts,
                &screen_verts,
                &args,
                render_ctx,
            );
        }
    }
}

/// Returns a 2D bounding box for `verts`.  If `clamp_bounds` contains a value, the resulting BB is
/// clamped to be within those bounds.
///
/// The first vector of the result is the bottom left.  The second is the top right.
fn bounding_box(verts: &[Vec2f; 3], clamp_bounds: Option<[Vec2u; 2]>) -> [Vec2u; 2] {
    let cmp_fn = |a: &f32, b: &f32| a.partial_cmp(b).unwrap();
    let min_x = verts.iter().map(|v| v.x).min_by(cmp_fn).unwrap().floor() as u32;
    let min_y = verts.iter().map(|v| v.y).min_by(cmp_fn).unwrap().floor() as u32;
    let max_x = verts.iter().map(|v| v.x).max_by(cmp_fn).unwrap().ceil() as u32;
    let max_y = verts.iter().map(|v| v.y).max_by(cmp_fn).unwrap().ceil() as u32;
    match clamp_bounds {
        Some(clamp_bounds) => {
            [
                Vec2u {
                    x: min_x.clamp(clamp_bounds[0].x, clamp_bounds[1].x),
                    y: min_y.clamp(clamp_bounds[0].y, clamp_bounds[1].y),
                },
                Vec2u {
                    x: max_x.clamp(clamp_bounds[0].x, clamp_bounds[1].x),
                    y: max_y.clamp(clamp_bounds[0].y, clamp_bounds[1].y),
                },
            ]
        }
        None => {
            [
                Vec2u {
                    x: min_x,
                    y: min_y,
                },
                Vec2u {
                    x: max_x,
                    y: max_y,
                },
            ]
        }
    }
}

fn point_in_triangle(
    x: u32,
    y: u32,
    clip_verts: &[Vec4f; 3],
    screen_verts: &[Vec2f; 3],
    args: &FragShaderArgs,
    render_ctx: &mut RenderContext,
) {
    let p = Vec2f::new(x as f32, y as f32);
    let bary_coords = barycentric(&p, screen_verts);
    // Check if `p` is inside the triangle.
    if AsRef::<[f32; 3]>::as_ref(&bary_coords)
        .iter()
        .all(|&v| v >= 0.0)
        {
            // It's inside.  Now check `z_buf` to see if `p` is occluded.
            let z = bary_coords.dot(Vec3f::new(
                clip_verts[0].z,
                clip_verts[1].z,
                clip_verts[2].z,
            ));
            let z_buf_idx = (x * (render_ctx.target_width() as u32) + y) as usize;
            if z > render_ctx.z_buf[z_buf_idx] {
                // `p` is visible.  Run the context's fragment shader.
                if let Some(frag_col) = render_ctx.shader.fragment(&bary_coords, args) {
                    // The fragment wasn't discarded, so we update `z_buf`.
                    render_ctx.z_buf[z_buf_idx] = z;

                    let frag_col = frag_col.map(|v| v.clamp(0.0, 1.0));
                    // Map components of `frag_col` from [0.0, 1.0] to [0, 255].
                    let screen_col = image::Rgba([
                        (255.0 * frag_col[0]).round() as u8,
                        (255.0 * frag_col[1]).round() as u8,
                        (255.0 * frag_col[2]).round() as u8,
                        255,
                    ]);

                    render_ctx.put_pixel(x as u32, y as u32, screen_col);
                }
            }
        }
}

pub fn line(verts: [Vec2i; 2], color: Color, render_ctx: &mut RenderContext) {
    let mut steep = false;
    let verts = if (verts[0].x - verts[1].x).abs() < (verts[0].y - verts[1].y).abs() {
        steep = true;
        [
            Vec2i {
                x: verts[0].y,
                y: verts[0].x,
            },
            Vec2i {
                x: verts[1].y,
                y: verts[1].x,
            },
        ]
    } else {
        verts
    };

    let verts = if verts[0].x > verts[1].x {
        [verts[1], verts[0]]
    } else {
        verts
    };
    let dx = verts[1].x - verts[0].x;
    let dy = verts[1].y - verts[0].y;
    let derror2 = 2 * dy.abs();
    let mut error2 = 0;
    let mut y = verts[0].y;
    for x in verts[0].x..=verts[1].x {
        if steep {
            render_ctx.put_pixel(y as u32, x as u32, color);
        } else {
            render_ctx.put_pixel(x as u32, y as u32, color);
        }
        error2 += derror2;
        if error2 > dx {
            if verts[1].y > verts[0].y {
                y += 1;
            } else {
                y -= 1;
            };
            error2 -= 2 * dx;
        }
    }
}
