pub mod primitive;
pub mod shader;

use cgmath::InnerSpace;
use image::RgbaImage;

use crate::camera::Camera;
use crate::color::*;
use crate::math::matrix;
use crate::math::types::*;
use crate::model::Model;
use self::primitive::*;
use self::shader::*;

const EYE: Point3f = Point3f {
    x: 0.1,
    y: 0.1,
    z: 0.7,
};
const CENTER: Point3f = Point3f {
    x: 0.0,
    y: 0.0,
    z: 0.0,
};
const UP: Vec3f = Vec3f {
    x: 0.0,
    y: 1.0,
    z: 0.0,
};
const LIGHT_POS: Point3f = Point3f {
    x: 0.5,
    y: 0.5,
    z: 0.5,
};

pub struct Renderer {
    depth_ctx: RenderContext,
    main_ctx: RenderContext,
    light_dir: Vec3f,
}

impl Renderer {
    pub fn new(width: u32, height: u32) -> Self {
        Self {
            depth_ctx: RenderContext::new(
                width, height,
                Box::new(DepthShader::new()),
                Camera::new(&LIGHT_POS, &CENTER, &UP),
                matrix::projection(0.0),
                matrix::viewport(width / 8, height / 8, width * 3 / 4, height * 3 / 4),
            ),
            main_ctx: RenderContext::new(
                width, height,
                Box::new(MainShader::new()),
                Camera::new(&EYE, &CENTER, &UP),
                matrix::projection(-1.0 / 10.0),
                matrix::viewport(width / 8, height / 8, width * 3 / 4, height * 3 / 4),
            ),
            light_dir: Vec3f::new(1.0, 1.0, 1.0).normalize(),
        }
    }

    pub fn render(&mut self, model: &Model) {
        //Self::render_pass(&self.light_dir, &mut self.depth_ctx, model);
        Self::render_pass(&self.light_dir, &mut self.main_ctx, model);
    }

    fn render_pass(light_dir: &Vec3f, render_ctx: &mut RenderContext, model: &Model) {
        render_ctx.shader.set_uniforms(light_dir, &render_ctx.proj_mat, &render_ctx.camera, model);

        for face in model.faces() {
            let clip_positions: [Vec4f; 3] = array_init::array_init(|i| {
                render_ctx.shader.vertex(i, &face.vertices[i])
            });

            triangle(
                &clip_positions,
                FragShaderArgs {
                    diffuse_tex: model.diffuse_texture(),
                    normal_map: model.normal_map(),
                    specular_map: model.specular_map(),
                },
                render_ctx,
            );
        }
    }

    pub fn save(&self, file_path: &str) {
        self.main_ctx.render_target.save(file_path);
    }

    pub fn screen_width(&self) -> u32 {
        self.main_ctx.render_target.width()
    }

    pub fn screen_height(&self) -> u32 {
        self.main_ctx.render_target.height()
    }
}

pub struct RenderContext {
    render_target: RenderSurface,
    z_buf: Vec<f32>,
    shader: Box<dyn Shader>,
    camera: Camera,
    /// Maps points from camera space onto screen coordinates.
    proj_mat: Mat4f,
    /// Scales and translates points to occupy a certain region of the screen.
    viewport_mat: Mat4f,
}

impl RenderContext {
    pub fn new(width: u32, height: u32, shader: Box<Shader>, camera: Camera, proj_mat: Mat4f, viewport_mat: Mat4f) -> Self {
        Self {
            render_target: RenderSurface::new(width, height),
            z_buf: vec![std::f32::MIN; (width * height) as usize],
            shader,
            camera,
            proj_mat,
            viewport_mat,
        }
    }

    pub fn put_pixel(&mut self, x: u32, y: u32, color: Color) {
        self.render_target.put_pixel(x, y, color)
    }

    pub fn target_width(&self) -> u32 { self.render_target.width() }

    pub fn target_height(&self) -> u32 { self.render_target.height() }

    pub fn target_bounds(&self) -> [Vec2u; 2] {
        [
            Vec2u::new(0, 0),
            Vec2u::new(self.target_width(), self.target_height()),
        ]
    }
}

pub struct RenderSurface {
    surface: RgbaImage,
}

impl RenderSurface {
    pub fn new(width: u32, height: u32) -> Self {
        Self {
            surface: RgbaImage::from_fn(width, height, |_, _| BLACK),
        }
    }

    /// Places a pixel on `screen`, if the points are within `screen`s bounds.
    pub fn put_pixel(&mut self, x: u32, y: u32, color: Color) {
        // We don't need to check if they're greater than 0, because they're unsigned.
        if x < self.width() && y < self.height() {
            self.surface.put_pixel(x, y, color);
        }
    }

    pub fn save(&self, file_path: &str) {
        // We've been working with the origin at the *bottom* left, but `image` assumes it's at the
        // *top* left.  So we flip the entire screen before writing.
        let flipped_target = image::imageops::flip_vertical(&self.surface);
        flipped_target.save(file_path).unwrap();
    }

    pub fn width(&self) -> u32 { self.surface.width() }

    pub fn height(&self) -> u32 { self.surface.height() }
}
