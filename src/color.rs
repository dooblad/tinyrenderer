const fn make_color(data: [u8; 4]) -> Color {
    Color { data }
}
pub const RED: Color = make_color([255, 0, 0, 255]);
pub const GREEN: Color = make_color([0, 255, 0, 255]);
pub const BLUE: Color = make_color([0, 0, 255, 255]);
pub const BLACK: Color = make_color([0, 0, 0, 255]);
pub const WHITE: Color = make_color([255, 255, 255, 255]);

pub type Color = image::Rgba<u8>;
