pub mod types {
    pub type Point2i = cgmath::Point2<i32>;
    pub type Point2u = cgmath::Point2<u32>;
    pub type Point2f = cgmath::Point2<f32>;
    pub type Point3i = cgmath::Point3<i32>;
    pub type Point3u = cgmath::Point3<u32>;
    pub type Point3f = cgmath::Point3<f32>;

    pub type Vec2i = cgmath::Vector2<i32>;
    pub type Vec2u = cgmath::Vector2<u32>;
    pub type Vec2f = cgmath::Vector2<f32>;
    pub type Vec3i = cgmath::Vector3<i32>;
    pub type Vec3u = cgmath::Vector3<u32>;
    pub type Vec3f = cgmath::Vector3<f32>;
    pub type Vec4i = cgmath::Vector4<i32>;
    pub type Vec4u = cgmath::Vector4<u32>;
    pub type Vec4f = cgmath::Vector4<f32>;

    pub type Mat3f = cgmath::Matrix3<f32>;
    pub type Mat4f = cgmath::Matrix4<f32>;
}

pub mod matrix {
    use cgmath::{BaseFloat, InnerSpace};

    use super::types::*;

    pub trait RowMajorMatrix3Construct<S: BaseFloat> {
        /// Constructs a `Matrix3` in row-major form, like a fucking sane human being would.
        #[rustfmt::skip]
        fn row_maj_new(
            r0c0: S, r0c1: S, r0c2: S,
            r1c0: S, r1c1: S, r1c2: S,
            r2c0: S, r2c1: S, r2c2: S,
        ) -> cgmath::Matrix3<S>;
    }

    impl<S: BaseFloat> RowMajorMatrix3Construct<S> for cgmath::Matrix3<S> {
        /// Constructs a `Matrix3` in row-major form, like a fucking sane human being would.
        #[rustfmt::skip]
        fn row_maj_new(
            r0c0: S, r0c1: S, r0c2: S,
            r1c0: S, r1c1: S, r1c2: S,
            r2c0: S, r2c1: S, r2c2: S,
        ) -> cgmath::Matrix3<S> {
            cgmath::Matrix3::new(
                r0c0, r1c0, r2c0,
                r0c1, r1c1, r2c1,
                r0c2, r1c2, r2c2,
            )
        }
    }

    pub trait RowMajorMatrix4Construct<S: BaseFloat> {
        /// Constructs a `Matrix4` in row-major form, like a fucking sane human being would.
        #[rustfmt::skip]
        fn row_maj_new(
            r0c0: S, r0c1: S, r0c2: S, r0c3: S,
            r1c0: S, r1c1: S, r1c2: S, r1c3: S,
            r2c0: S, r2c1: S, r2c2: S, r2c3: S,
            r3c0: S, r3c1: S, r3c2: S, r3c3: S,
        ) -> cgmath::Matrix4<S>;
    }

    impl<S: BaseFloat> RowMajorMatrix4Construct<S> for cgmath::Matrix4<S> {
        /// Constructs a `Matrix4` in row-major form, like a fucking sane human being would.
        #[rustfmt::skip]
        fn row_maj_new(
            r0c0: S, r0c1: S, r0c2: S, r0c3: S,
            r1c0: S, r1c1: S, r1c2: S, r1c3: S,
            r2c0: S, r2c1: S, r2c2: S, r2c3: S,
            r3c0: S, r3c1: S, r3c2: S, r3c3: S,
        ) -> cgmath::Matrix4<S> {
            cgmath::Matrix4::new(
                r0c0, r1c0, r2c0, r3c0,
                r0c1, r1c1, r2c1, r3c1,
                r0c2, r1c2, r2c2, r3c2,
                r0c3, r1c3, r2c3, r3c3,
            )
        }
    }

    /// Produces a matrix that transforms from normalized device coordinates to screen coordinates,
    /// where `x`, `y`, `w`, and `h` define the size and location of the viewport on the screen,
    /// where the points should be mapped to.
    #[rustfmt::skip]
    pub fn viewport(x: u32, y: u32, w: u32, h: u32) -> Mat4f {
        let w_2 = w as f32 / 2.0;
        let h_2 = h as f32 / 2.0;
        Mat4f::row_maj_new(
            w_2, 0.0, 0.0, x as f32 + w_2,
            0.0, h_2, 0.0, y as f32 + h_2,
            0.0, 0.0, 1.0, 0.0,
            0.0, 0.0, 0.0, 1.0,
        )
    }

    // TODO: Make the parameter FOV-based.
    /// Produces a matrix that transforms from view space coordinates to clip space coordinates.
    #[rustfmt::skip]
    pub fn projection(inv_c: f32) -> Mat4f {
        Mat4f::row_maj_new(
            1.0, 0.0, 0.0, 0.0,
            0.0, 1.0, 0.0, 0.0,
            0.0, 0.0, 1.0, 0.0,
            0.0, 0.0, inv_c, 1.0,
        )
    }

    pub fn camera(eye: &Point3f, center: &Point3f, up: &Vec3f) -> Mat4f {
        let z = (eye - center).normalize();
        let x = up.cross(z).normalize();
        let y = z.cross(x).normalize();
        #[rustfmt::skip]
        let m_inv = Mat4f::row_maj_new(
            x.x, x.y, x.z, 0.0,
            y.x, y.y, y.z, 0.0,
            z.x, z.y, z.z, 0.0,
            0.0, 0.0, 0.0, 1.0,
        );
        #[rustfmt::skip]
        let translate = Mat4f::row_maj_new(
            1.0, 0.0, 0.0, -center.x,
            0.0, 1.0, 0.0, -center.y,
            0.0, 0.0, 1.0, -center.z,
            0.0, 0.0, 0.0, 1.0,
        );
        m_inv * translate
    }
}

pub mod vector {
    use cgmath::InnerSpace;

    use super::types::*;

    pub fn barycentric(point: &Vec2f, verts: &[Vec2f; 3]) -> Vec3f {
        // TODO: Does ignoring the z coordinate give us accurate barycentric coords?
        let alpha_numer = (verts[1].y - verts[2].y) * (point.x - verts[2].x)
            + (verts[2].x - verts[1].x) * (point.y - verts[2].y);
        let beta_numer = (verts[2].y - verts[0].y) * (point.x - verts[2].x)
            + (verts[0].x - verts[2].x) * (point.y - verts[2].y);
        let denom = (verts[1].y - verts[2].y) * (verts[0].x - verts[2].x)
            + (verts[2].x - verts[1].x) * (verts[0].y - verts[2].y);
        let alpha = alpha_numer / denom;
        let beta = beta_numer / denom;
        let gamma = 1.0 - alpha - beta;
        Vec3f::new(alpha, beta, gamma)
    }

    /// Returns the result of reflecting `to_reflect` about the `about` vector.
    pub fn reflect(to_reflect: &Vec3f, about: &Vec3f) -> Vec3f {
        // Computes the projection of `to_reflect` onto `about` with double the length (call this
        // projection `p`), then subtracts `to_reflect` from the double-length projection.
        //
        // In the coordinate frame of `about`, subtracting `to_reflect` from `p` leaves the result
        // at the same height as `to_reflect`, and the same x-distance away from `about` as
        // `to_reflect`.
        2.0 * about * (about.dot(*to_reflect)) - to_reflect
    }
}

pub trait Clamp {
    fn clamp(&self, low: Self, high: Self) -> Self;
}

impl Clamp for f32 {
    fn clamp(&self, low: f32, high: f32) -> f32 {
        if *self < low {
            low
        } else if *self > high {
            high
        } else {
            *self
        }
    }
}

impl Clamp for u32 {
    fn clamp(&self, low: u32, high: u32) -> u32 {
        if *self < low {
            low
        } else if *self > high {
            high
        } else {
            *self
        }
    }
}
