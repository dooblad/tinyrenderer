use crate::math::types::*;

pub struct Texture {
    data: Vec<Vec4f>,
    width: u32,
    height: u32,
}

impl Texture {
    // TODO: It would be way better if we had an enum of texture types that each had stored their
    // data in a different format.  Like... right now, we're just duplicating the luma value for
    // each RGB component for grayscale textures.
    pub fn load(filename: &str) -> Self {
        // Load the raw image data.
        let raw_image = {
            let result = match image::open(filename).unwrap() {
                image::DynamicImage::ImageRgba8(tex) => tex,
                image::DynamicImage::ImageRgb8(tex) => {
                    // Gotta diddle it into the right format.
                    let width = tex.width();
                    let height = tex.height();
                    let mut rgba_vec: Vec<u8> = Vec::with_capacity((width * height * 4) as usize);
                    for col in tex.into_vec().chunks(3) {
                        rgba_vec.append(&mut vec![col[0], col[1], col[2], 255]);
                    }
                    image::ImageBuffer::from_vec(width, height, rgba_vec).unwrap()
                }
                image::DynamicImage::ImageLuma8(tex) => {
                    // Gotta diddle it into the right format.
                    let width = tex.width();
                    let height = tex.height();
                    let mut rgba_vec: Vec<u8> = Vec::with_capacity((width * height * 4) as usize);
                    for col in tex.into_vec() {
                        rgba_vec.append(&mut vec![col, col, col, 255]);
                    }
                    image::ImageBuffer::from_vec(width, height, rgba_vec).unwrap()
                }
                _ => panic!("unsupported image format"),
            };
            image::imageops::flip_vertical(&result)
        };
        // Transform the pixel values into the range [0, 1].
        let tex_data = raw_image
            .pixels()
            .map(|pix| {
                Vec4f::new(
                    pix[0] as f32 / 255.0,
                    pix[1] as f32 / 255.0,
                    pix[2] as f32 / 255.0,
                    pix[3] as f32 / 255.0,
                )
            })
            .collect();
        Self {
            data: tex_data,
            width: raw_image.width(),
            height: raw_image.height(),
        }
    }

    pub fn sample(&self, coord: Vec2f) -> Vec4f {
        let round_coord = Vec2u::new(
            (coord.x * (self.width as f32)).round() as u32 % self.width,
            (coord.y * (self.height as f32)).round() as u32 % self.height,
        );
        let data_idx = (round_coord.y * self.width + round_coord.x) as usize;
        self.data[data_idx].clone()
    }
}
