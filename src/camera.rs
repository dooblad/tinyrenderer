use crate::math::matrix;
use crate::math::types::*;

pub struct Camera {
    /// Maps points from model space to camera space (where the point (0, 0) is at the center of
    /// the screen).
    view_mat: Mat4f,
}

impl Camera {
    pub fn new(eye: &Point3f, center: &Point3f, up: &Vec3f) -> Self {
        Self {
            view_mat: matrix::camera(eye, center, up),
        }
    }

    pub fn view_mat(&self) -> &Mat4f {
        &self.view_mat
    }
}
